package com.example.Entreprise.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.Entreprise.modele.Adresse;
import com.example.Entreprise.service.AdresseService;
import lombok.AllArgsConstructor;
import java.util.List;

@RestController
@RequestMapping("/adresse")
@AllArgsConstructor
public class AdresseController {
    private final AdresseService adresseService;

    @PostMapping("/create")
    public Adresse create(@RequestBody Adresse adresse) {
        return adresseService.creer(adresse);
    }

    @GetMapping("/read")
    public List<Adresse> read() {
        return adresseService.lire();
    }

    @PutMapping("/update/{id}")
    public Adresse update(@PathVariable Long id, @RequestBody Adresse adresse) {
        return adresseService.modifier(id, adresse);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        return adresseService.supprimer(id);
    }

}
