package com.example.Entreprise.modele;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "SALARIES")
@Getter
@Setter
@NoArgsConstructor
public class Salarie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom_Salarie;
    private String prenom_salarie;
    private Long id_adresse;
    private Long id_service;
    private Long id_projet;

}