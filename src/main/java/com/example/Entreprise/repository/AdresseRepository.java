package com.example.Entreprise.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Entreprise.modele.Adresse;

public interface AdresseRepository extends JpaRepository<Adresse, Long> {
    

}
