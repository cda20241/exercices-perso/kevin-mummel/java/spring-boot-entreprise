package com.example.Entreprise.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.example.Entreprise.modele.Adresse;
import com.example.Entreprise.repository.AdresseRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AdresseServiceImpl implements AdresseService {

    private final AdresseRepository adresseRepository;

    @Override
    public Adresse creer(Adresse adresse) {
        return adresseRepository.save(adresse);
    }

    @Override
    public List<Adresse> lire() {
        return adresseRepository.findAll();
    }

    @Override
    public Adresse modifier(Long id, Adresse adresse) {
        return adresseRepository.findById(id)
                .map(adresse1 -> {

                    adresse1.setRue(adresse.getRue());
                    adresse1.setVille(adresse.getVille());
                    adresse1.setCode_postal(adresse.getCode_postal());
                    adresse1.setPays(adresse.getPays());

                    return adresseRepository.save(adresse1);
                }).orElseThrow(() -> new RuntimeException("Adresse non trouvée!"));
    }

    @Override
    public String supprimer(Long id) {
        adresseRepository.deleteById(id);
        return "Adresse supprimée!";
    }
}

