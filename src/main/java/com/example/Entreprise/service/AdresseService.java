package com.example.Entreprise.service;

import java.util.List;

import com.example.Entreprise.modele.Adresse;

public interface AdresseService {

    Adresse creer(Adresse adresse);

    List<Adresse> lire();

    Adresse modifier(Long id, Adresse adresse);

    String supprimer(Long id);


}
